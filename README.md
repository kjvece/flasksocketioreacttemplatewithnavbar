# Flask+SocketIO+React w/ Sidebar and Toasts Template

This template provides a quick structure for creating flask apps with a React frontend. The template also includes bootstrap 4.5 and its supporting files.

## Requirements

```
python3
pip3
nodejs
```

## Installation and running

#### Suggested: setup python3 virtual environment

If not already installed, use the following command to install the `venv` python3 module using `apt`

```
apt install python3-venv
```

Create a virtual environment named `env` using the command:

```
python3 -m venv env
```

Activate the environment using the command:

```
source env/bin/activate
```

### Installation

The easiest way to install dependencies is using `pip` and `npm`

```
pip install -r requirements.txt && npm i
```

### Running

By default, the app will run in development mode. To compile and run in production mode, use the command:

```
python app.py --no-debug
```

The bind address and port can be specified using the optional `--host` and `--port` arguments:

```
python app.py --host=127.0.0.1 --port=8000 --no-debug
```

### Accessing

By default, the app binds to 0.0.0.0:5000. Use your favorite webbrowser (not IE) to naviate to http://0.0.0.0:5000/

### For development

To run the app for development, use:

```
python app.y
```

If you are working on the React app, you can add the `--build` flag to rebuild the react app on change.

```
python app.py --build
```

## Flask

The `prelogin/` folder contains any files that can be accessed without authentication (such as the login page, and bootstrap). Contents in this folder can be changed to suit your needs (e.g. adding a registration page). The `serve` function maps all paths without extensions to `*.html` files by default (e.g. `/login` becomes `/login.html`) for a more modern presentation.

### Login

Login is handled via `Flask-Login`. Suggested validation method is to use or modify or replace the provided `prelogin/login.html` file to suit your application. Then edit the `validate_user` function in `app.py` to suit your validation schema.

### Using sqlalchemy

A sqlalchemy bind is already specified for session management. If you wish to use sqlalchemy as part of your backend (e.g. for user management), specify a `SQLALCHEMY_DATABASE_URI` in the `app.config` in `app.py`, as well as your corresponding `Model`s. Remember to specify a `__bind_key__` in your `Model` if you're using sqlalchemy binds instead of the default database.

### SocketIO

SocketIO is provided via the `Flask-SocketIO` plugin. Per the template, all sockets are handled in the `sockets.py` file. Default handlers/examples are provided for the `connect`, `disconnect` and `message` events.

The `@on` decorator should be used instead of the default `@socketio.on` decorator because it provides authentication for the `current_user`.

For convenience, the `SocketHandler` function also receives an instance of the sqlalchemy `db` object, if you happen to also be using sqlalchemy.

## React

All react files are hosted in `/src`. The main point of entry is `/src/App.js`, the `/src/index.js` and `/src/ContextProvider.js` files are not designed to be modified.

The `/public/index.html` file can be edited to change the title or other parts as necessary.

### Hooks

React hooks are methods to store reusable code among different components.

##### useSocket()

The `useSocketIO` hook is provided in `/src/hooks/SocketIO` to provide access to the global SocketIO object. See the default `App` component for example usage.


```javascript
const socket = useSocketIO();
```

##### useToast()

The `useToast` hook is provided in `/src/hooks/Toast` and returns a function that can be used to display a toast notification.

```jsx
export default function ToastButton() {
  const displayToast = useToast();

  return (
    <button onClick={() => {
        displayToast({
            icon: "favicon.ico",
            title: "toast notification",
            body: "it worked!",
        })
    }}>
        Display Toast
    </button>
  )
}
```

The `displayToast` function takes in an object as an argument with the following properties

|name|required|description|
|----|--------|-----------|
|`icon`|no|path to the icon to display in the titlebar|
|`title`|yes|title for the toast notification box|
|`titleStyle`|no|custom style to add to the title|
|`titleClass`|no|custom class override for the title|
|`body`|yes|body message to display in the toast notification|
|`bodyStyle`|no|custom style to add to the body|
|`bodyClass`|no|custom class override for the body|

##### useLogout()

The `useLogout` hook is provided in `/src/hooks/Logout` and returns a function that can be used to logout the current user.


```jsx
export default function LogoutComponent() {
  const logout = useLogout();

  return (
    <button className="btn btn-primary" onClick={logout}>
      Logout
    </button>
  )
}
```

### Sidebar

The items in the sidebar can be set in the `/src/App.js` file by adding an object to the `sidebarItems` array. Navigation is provided using `react-router-dom`. Therefore, external `Route`s can be manually added in the `/src/App.js` file if needed. It is suggested to use the `useHistory` hook from `react-router-dom` to access the `history` array and `history.push("/new-path")` if manual navigation is required (see `/src/components/Navbar` for example).


##### Example

```jsx
const sidebarItems = [
    {
        text: "Home",       // text to display in sidebar
        target: "/",        // route to use for this resource (exact)
        icon: faHome,       // font awesome icon to display
        component: Home,    // page component to render for this route
    },
    {
        text: "Help",
        target: "/help",
        icon: faQuestion,
        component: Help,
        bottom: true,       // optional parameter that moves this item to the bottom part of the sidebar
    },
];
```

### Toasts

Use the `useToast` hook from `/src/hooks/Toast.js` to get access to the `displayToast` method. See the Hooks section for more details on the hook.

Toasts are implemented using bootstraps toast, any options that can be used in bootstrap toasts can be passed into the `options` prop of the `Toast` component (e.g. `delay`, `autohide`).

### Contexts

React Contexts are a means to share state between disconnected components (e.g. a navbar and a page).

#### ContextProvider

For convenience, a context provider will automatically add all contexts in the `/src/contexts/` folder to the `App` component. In order for a Provider to be added, it MUST be named in the form `*Provider`. By convention, any contexts should export both the `Context` and `Provider` objects. For performance reasons, if your context provides an update function, it should be provided as a separate context.
