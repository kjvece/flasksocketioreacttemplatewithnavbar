import functools

from flask import session

from flask_login import current_user

from flask_socketio import emit
from flask_socketio import disconnect

def SocketHandler(db, socketio):

    def on(event):
        def decorator(fn):
            @functools.wraps(fn)
            def wrapper(*args, **kwargs):
                if not current_user.is_authenticated:
                    disconnect()
                    return
                return fn(*args, **kwargs)
            return socketio.on(event)(wrapper)
        return decorator

    @on("connect")
    def handle_connect():
        print(f"got connection from {current_user.id}")

    @on("disconnect")
    def handle_disconnect():
        print(f"got disconnect from {current_user.id}")

    @on("message")
    def handle_msg(msg):
        print(f"got message: {msg}")
        return f"echoing {current_user.id}: {msg}"
