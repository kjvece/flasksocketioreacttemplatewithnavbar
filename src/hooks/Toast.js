import { useContext } from "react";

import { ToastUpdateContext } from "../contexts/Toast";

export default function useToast() {
  const [addToast] = useContext(ToastUpdateContext);

  return addToast;
}
