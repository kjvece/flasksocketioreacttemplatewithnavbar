import { useContext } from "react";
import { SocketIOContext } from "../contexts/SocketIO";

export default function useSocketIO() {
    return useContext(SocketIOContext);
}
