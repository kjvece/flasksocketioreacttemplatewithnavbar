import React, { useState, useCallback } from "react";
import uuid from "react-uuid";

export const ToastContext = React.createContext();
export const ToastUpdateContext = React.createContext();

export function ToastProvider({ children }) {
    const [toasts, setToasts] = useState([]);

    const addToast = useCallback(toast => {
        setToasts(prevToasts => (
            [{...toast, id: uuid()}, ...prevToasts]
        ));
    }, []);

    const removeToast = useCallback(id => {
        setToasts(prevToasts => (
            prevToasts.filter(toast => toast.id !== id)
        ));
    }, []);

    return (
        <ToastContext.Provider value={toasts} >
          <ToastUpdateContext.Provider value={[addToast, removeToast]} >
            {children}
          </ToastUpdateContext.Provider>
        </ToastContext.Provider>
    )
}
