import React, { useReducer, useCallback } from "react";

export const NavbarContext = React.createContext();
export const NavbarUpdateContext = React.createContext();

export const NavbarACTIONS = {
    SET_ITEMS: "set_items",
    ADD_TOP_ITEM: "add_top",
    ADD_BOTTOM_ITEM: "add_top",
    INC_BADGE: "inc_badge",
    CLEAR_BADGE: "clear_badge",
}

function reducer(state, action) {
    switch(action.type) {
        case NavbarACTIONS.SET_ACTIVE:
            return {
                ...state,
                active: action.text,
            };
        case NavbarACTIONS.SET_ITEMS:
            return {
                active: action.items[0].text,
                items: [...action.items]
            };
        case NavbarACTIONS.ADD_TOP_ITEM:
            var { type, ...rest } = action;
            return {
                ...state,
                items: [...state.items, {...rest, bottom: false }]
            }
        case NavbarACTIONS.ADD_BOTTOM_ITEM:
            var { type, ...rest } = action;
            return {
                ...state,
                items: [...state.items, {...rest, bottom: true }]
            }
        case NavbarACTIONS.INC_BADGE:
            if (action.text === state.active) return state;
            return {
                ...state,
                items: state.items.map(item => {
                    if (item.text === action.text) {
                        if (item.badge)
                            return { ...item, badge: item.badge + 1 };
                        else
                            return { ...item, badge: 1 };
                    }
                    return item;
                }),
            };
        case NavbarACTIONS.CLEAR_BADGE:
            return {
                ...state,
                items: state.items.map(item => {
                    if (item.text === action.text) {
                        return { ...item, badge : undefined };
                    }
                    return item;
                }),
            }
        default:
            return state;
    }
}

export function NavBarProvider({ children }) {
    const [state, dispatch] = useReducer(reducer, {active: undefined, items: []});

    return (
        <NavbarContext.Provider value={state} >
          <NavbarUpdateContext.Provider value={dispatch} >
            {children}
          </NavbarUpdateContext.Provider>
        </NavbarContext.Provider>
    )
}
