import React from "react";
import ReactDOM from "react-dom";
import ContextProvider from "./ContextProvider";
import App from "./App";

import "./components/Navbar.css";

ReactDOM.render(
  <React.StrictMode>
    <ContextProvider>
     <App />
    </ContextProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
