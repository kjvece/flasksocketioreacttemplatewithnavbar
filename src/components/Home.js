import React, { useState } from "react";

import useToast from "../hooks/Toast";
import useSocket from "../hooks/SocketIO";

export default function Home() {
  const displayToast = useToast();
  const socket = useSocket();

  const [text, setText] = useState("");

  return (
  <>
    <h2>Home</h2>
    <div className="input-group mb-3">
    <div className="input-group-prepend">
    <input type="text" name="text" placeholder="enter message" onChange={(e) => setText(e.target.value)} />
    </div>
    <button
        className="btn btn-primary"
        onClick={() => socket.send(text, (msg) => {
            displayToast({
                title: "Received echo from server",
                body: msg
            });
        })}
    >
        Echo Message
    </button>
    </div>
  </>
  )
}
