import React, { useCallback, useContext } from "react";
import { useHistory } from "react-router-dom";

import { NavbarUpdateContext, NavbarACTIONS } from "../contexts/Navbar";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function NavbarItem({
    item,
    theme,
    clickHandler,
    tooltip,
}) {

    const history = useHistory();

    const updateNavbar = useContext(NavbarUpdateContext);

    const { icon, text, target, badge } = item;

    const handleClick = useCallback(() => {
        updateNavbar({ type: NavbarACTIONS.CLEAR_BADGE, text });
        updateNavbar({ type: NavbarACTIONS.SET_ACTIVE, text });
        history.push(target);
    }, [text, target]);

    return (
     <>
      <button
       className={`list-group-item list-group-item-action btn ${theme}`}
       style={{
         boxShadow: "none",
       }}
       onClick={ clickHandler? clickHandler : handleClick }
       data-toggle={tooltip && "tooltip"}
       data-placement={tooltip && "right"}
       title={tooltip && text}
      >
       <span className="sidebar-icon">
         <FontAwesomeIcon icon={icon} />
       </span>
       <span className="sidebar-text">{text}</span>
       {
           badge && <span style={{
               position: "absolute",
               top: 5,
               right: 2,
           }} className="sidebar-badge badge badge-pill badge-danger">{badge}</span>
       }
      </button>
     </>
    )

}
