import React from "react";
import { faSignOutAlt } from "@fortawesome/free-solid-svg-icons";

import useLogout from "../hooks/Logout";

import NavbarItem from "./NavbarItem";

export default function Navbar({
    sidebarItems,
    showArrow,
    tooltip,
}) {

    const logout = useLogout();

    const theme = "text-light bg-dark nav-dark";

    const verticalFlex = "d-flex flex-column justify-content-between";

    const sidebarClass = true?
        `active ${theme} ${verticalFlex}`:
        `${theme} ${verticalFlex}`;

    return (
      <>
       <nav id="sidebar" className={sidebarClass}>
        <div className="list-group list-group-flush">
          {sidebarItems.filter(item => !item.bottom).map(item => (
           <NavbarItem
             item={item}
             theme={theme}
             tooltip={tooltip}
           />
          ))}
        </div>
        <div className="list-group list-group-flush">
          {sidebarItems.filter(item => item.bottom).map(item => (
           <NavbarItem
             item={item}
             theme={theme}
             tooltip={tooltip}
           />
          ))}
          <NavbarItem
            clickHandler={logout}
            item={{
                icon: faSignOutAlt,
                text: "Logout",
            }}
            theme={theme}
            tooltip={tooltip}
          />
        </div>
       </nav>
     </>
    )
}
