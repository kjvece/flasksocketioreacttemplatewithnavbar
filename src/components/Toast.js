import React, { useEffect, useContext } from "react";
import $ from "jquery";

import { ToastContext, ToastUpdateContext } from "../contexts/Toast";

export default function Toast({ options }) {

    const toasts = useContext(ToastContext);
    const [, removeToast] = useContext(ToastUpdateContext);

    useEffect(() => {
        $(".toast").toast(options);
        $(".toast").toast("show");
        function removeToastWrapper(e) {
            removeToast(e.target.id);
        }
        $(".toast").on("hidden.bs.toast", removeToastWrapper);

        return () => {
          $(".toast").off("hidden.bs.toast", removeToastWrapper);
        }

    }, [options, removeToast, toasts]);

    return (
        <div
            aria-live="polite"
            aria-atomic="true"
            style={{
                position: "fixed",
                top: "5px",
                right: "5px",
                width: "30%",
                minHeight: "200px"
            }}
        >
          <div
            style={{
                position: "absolute",
                top: 0,
                right: 0,
                width: "100%"
            }}
          >

            {
              toasts.map(toast => (
                <div
                    className="toast"
                    role="alert"
                    aria-live="assertive"
                    aria-atomic="true"
                    style={{
                        marginLeft: "auto",
                        width: "100%"
                    }}
                    id={toast.id}
                    key={toast.id}
                 >
                  <div className="toast-header">
                    {
                        toast.icon &&
                        <img
                            src={toast.icon}
                            className="rounded mr-2"
                            style={{width: "20px", height: "20px"}}
                            alt="..."
                        />
                    }
                    <strong
                        className={toast.titleClass || "mr-auto"}
                        style={toast.titleStyle}
                    >
                        {toast.title}
                    </strong>
                    <button
                        type="button"
                        className="ml-2 mb-1 close"
                        data-dismiss="toast"
                        aria-label="Close"
                     >
                        <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div className="toast-body">
                    <span
                      style={toast.bodyStyle}
                      className={toast.bodyClass}
                    >
                      {toast.body}
                    </span>
                  </div>
                </div>
              ))
            }

          </div>
        </div>
    )
}
