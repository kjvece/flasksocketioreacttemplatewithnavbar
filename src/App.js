import React, { useEffect, useContext } from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";

import "bootstrap";

import useSocketIO from "./hooks/SocketIO";
import useToast from "./hooks/Toast";

import Navbar from "./components/Navbar";
import Home from "./components/Home";
import Help from "./components/Help";
import Settings from "./components/Settings";

import Toast from "./components/Toast";

import {
    NavbarContext,
    NavbarUpdateContext,
    NavbarACTIONS,
} from "./contexts/Navbar";

import {
    faHome,
    faQuestion,
    faCog,
} from "@fortawesome/free-solid-svg-icons";

function App() {
  const socket = useSocketIO();
  const displayToast = useToast();

  const { items } = useContext(NavbarContext);
  const updateSidebar = useContext(NavbarUpdateContext);

  useEffect(() => {
      function handleConnect() {
        displayToast({
            title: "Notification",
            titleClass: "mr-auto text-success",
            body: "socket.io connected!"
        });
      }
      function handleDisconnect() {
        displayToast({
            title: "Alert",
            titleClass: "mr-auto text-danger",
            body: "socket.io disconnected!",
        });
      }
      socket.on("connect", handleConnect);
      socket.on("disconnect", handleDisconnect);
      return () => {
          socket.off("connect", handleConnect);
          socket.off("disconnect", handleDisconnect);
      }
  }, [socket, displayToast]);

  useEffect(() => {
      updateSidebar({
          type: NavbarACTIONS.SET_ITEMS,
          items: [
              {
                  text: "Home",
                  target: "/",
                  icon: faHome,
                  component: Home,
              },
              {
                  text: "Settings",
                  target: "/settings",
                  icon: faCog,
                  component: Settings,
                  bottom: true,
              },
              {
                  text: "Help",
                  target: "/help",
                  icon: faQuestion,
                  component: Help,
                  bottom: true,
              },
          ]
      });
  }, [updateSidebar]);

  useEffect(() => {
      function incBadge() {
          updateSidebar({
              type: NavbarACTIONS.INC_BADGE,
              text: "Home"
          });
      }
      const inc = setInterval(incBadge, 3000);
      return () => clearInterval(inc);
  }, [updateSidebar]);

  return (
   <Router>
    <Toast options={{delay: 5000}} />
    <div id="wrapper" className="d-flex">
        <Navbar sidebarItems={items} tooltip />
        <div id="content">
          <Switch>
            {items.map(item => (
              <Route
                path={item.target}
                exact
                component={item.component}
              />
            ))}
          </Switch>
        </div>
    </div>
   </Router>
  );
}

export default App;
